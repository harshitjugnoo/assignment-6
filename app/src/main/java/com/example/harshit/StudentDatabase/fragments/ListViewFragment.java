package com.example.harshit.StudentDatabase.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.example.harshit.StudentDatabase.R;
import com.example.harshit.StudentDatabase.activities.AddStudent;
import com.example.harshit.StudentDatabase.activities.MainActivity;
import com.example.harshit.StudentDatabase.adapters.MyCustomAdapter;
import com.example.harshit.StudentDatabase.utils.AppConstants;

import static com.example.harshit.StudentDatabase.activities.MainActivity.studentList;

/**
 * Created by Harshit on 2/23/2015.
 */
public class ListViewFragment extends Fragment implements AppConstants {

    public ListView listView;
    private Button viewOption, editOption, deleteOption;
    private OnListPass listPasser;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_view, container, false);
        listView = (ListView) view.findViewById(R.id.list_view);

        if (getActivity() instanceof MainActivity) {
            ((MainActivity) getActivity()).new AsyncGetView().execute(VIEW_ALL);
        }
        if (getArguments() != null) {
            listView.setAdapter((MyCustomAdapter) getArguments().getSerializable(ADAPTER));
            listView.setTextFilterEnabled(true);
            listPasser.onListPass(listView);
        }

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                itemClick(parent, view, position, id);//function to display the dialog box
            }
        });

        return view;
    }


    public interface OnListPass {
        public void onListPass(ListView listView1);
    }

    @Override
    public void onAttach(Activity a) {
        super.onAttach(a);
        listPasser = (OnListPass) a;
    }




    private void itemClick(AdapterView<?> parent, View view, final int position, long id) {
        final Dialog dialog = new Dialog(getActivity()); //create the Dialog class object
        dialog.setContentView(R.layout.custom_dialog);
        dialog.setTitle(CHOOSE_OPTION); //Sets the title of the dialog
        viewOption = (Button) dialog.findViewById(R.id.viewOption);
        editOption = (Button) dialog.findViewById(R.id.editOption);
        deleteOption = (Button) dialog.findViewById(R.id.deleteOption);

        //for viewing details of a student
        viewOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent view = new Intent(getActivity(), AddStudent.class);
                //The roll number is used to get data from database
                String rollNo = studentList.get(position).getRollNo();
                view.putExtra(ROLL_NO, rollNo);// So it is sent to the AddStudent activity
                view.putExtra(BUTTON_CLICKED, VIEW_STUDENT); //Bundle up the button which is clicked
                startActivity(view);
                dialog.dismiss();
            }
        });

        //for editing the details of the student
        editOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent edit = new Intent(getActivity(), AddStudent.class);
                //The roll number is used to get data from database
                String rollNo = studentList.get(position).getRollNo();
                edit.putExtra(ROLL_NO, rollNo);
                edit.putExtra(BUTTON_CLICKED, EDIT_STUDENT);//Bundle up the button which is clicked
                getActivity().startActivityForResult(edit, EDIT_REQUEST_CODE);
                dialog.dismiss();
            }
        });

        //for deleting the student
        deleteOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //The roll number is used to get data from database
                String rollNo = studentList.get(position).getRollNo();
                ((MainActivity) getActivity()).new AsyncGetView().execute(DELETE_STUDENT, rollNo,
                        Integer.toString(position));
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}
