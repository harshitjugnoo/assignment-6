package com.example.harshit.StudentDatabase.activities;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.harshit.StudentDatabase.R;
import com.example.harshit.StudentDatabase.entities.Student;
import com.example.harshit.StudentDatabase.utils.AppConstants;
import com.example.harshit.StudentDatabase.utils.DbController;

import java.io.Serializable;

public class AddStudent extends Activity implements Serializable, AppConstants, OnClickListener {

    private Bundle extras;
    private TextView activityTitle;
    private ProgressDialog pDialog;
    private Button saveButton, cancelButton;
    String name, rollNo, deptt, contact, address;
    private EditText userName, userRollNo, userDeptt, userContact, userAddress;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.addstudent);
        userName = (EditText) findViewById(R.id.userName);
        userRollNo = (EditText) findViewById(R.id.userRollNo);
        userDeptt = (EditText) findViewById(R.id.user_deptt);
        userContact = (EditText) findViewById(R.id.user_contact);
        userAddress = (EditText) findViewById(R.id.user_address);
        saveButton = (Button) findViewById(R.id.save_button);
        cancelButton = (Button) findViewById(R.id.cancel_button);
        activityTitle = (TextView) findViewById(R.id.subheading);
        getExtras(); // This function extracts the bundle
        saveButton.setOnClickListener(this);
        cancelButton.setOnClickListener(this);
    }

    //This function extracts the bundle and performs the necessary operation
    private void getExtras() {
        extras = getIntent().getExtras(); // Extract the bundle into extras
        if (extras != null) {
//If View Student option is selected from the dialog box, get details from the database asynchronously
            if (extras.get(BUTTON_CLICKED).equals(VIEW)) { //If view option is selected
                String rollNo = (String) extras.get(ROLL_NO); //Roll number is used to fetch the details
                new AsyncProcess().execute(GET_STUDENT, rollNo);//Get the details of the particular student from database
                userName.setEnabled(false);//This is meant to keep the fields non editable
                userRollNo.setEnabled(false);
                userDeptt.setEnabled(false);
                userContact.setEnabled(false);
                userAddress.setEnabled(false);
                activityTitle.setText(HEADING);
                saveButton.setVisibility(View.INVISIBLE);//Keep the save button hidden
                cancelButton.setText(BACK);//Cancel button is changed to Back button
            }
            //If Edit Student option is selected from the dialog box, get details from the database asynchronously
            else if (extras.get(BUTTON_CLICKED).equals(EDIT)) {
                String rollNo = (String) extras.get(ROLL_NO);
                new AsyncProcess().execute(EDIT_STUDENT, rollNo);//Get the details of the particular student from database
                userRollNo.setEnabled(false);//The roll number cannot be changed once assigned
                activityTitle.setText(HEADER);
                saveButton.setText(UPDATE);//Save button is changed to Update button
            }
        }
    }

    //Shows the Progress Bar Dialog
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case PROGRESS_BAR:
                pDialog = new ProgressDialog(this);
                pDialog.setMessage(PLEASE_WAIT);
                pDialog.setIndeterminate(false);
                pDialog.setMax(100);
                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pDialog.setCancelable(true);
                pDialog.show();
                return pDialog;
            default:
                return null;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.save_button:
                name = userName.getText().toString();
                rollNo = userRollNo.getText().toString();
                deptt = userDeptt.getText().toString();
                contact = userContact.getText().toString();
                address = userAddress.getText().toString();
                //If fields are left empty, notify user to fill them
                if (name.equals(EMPTY) || rollNo.equals(EMPTY) || deptt.equals(EMPTY) ||
                        contact.equals(EMPTY) || address.equals(EMPTY)) {
                    Toast.makeText(AddStudent.this, FILL_EACH_FIELD, Toast.LENGTH_SHORT).show();
                }
                //If roll number is entered as Zero, notify user that it cannot be Zero
                else if ((Integer.parseInt(rollNo) == 0)) {
                    Toast.makeText(AddStudent.this, ROLL_NO_CONDITION, Toast.LENGTH_SHORT).show();
                }
                //If only spaces are entered in fields, request user to remove them
                else if (name.indexOf(SPACE) == 0 || deptt.indexOf(SPACE) == 0 ||
                        contact.indexOf(SPACE) == 0 || address.indexOf(SPACE) == 0) {
                    Toast.makeText(AddStudent.this, AVOID_LEADING_SPACES, Toast.LENGTH_SHORT).show();
                }
                //If all fields are entered correctly, proceed with the selected operation with the help of Async Task
                else {
                    //If add student option is selected, create a student entry inside the database
                    if (extras != null && extras.get(BUTTON_CLICKED).equals(ADD_STUDENT)) {
                        new AsyncProcess().execute(CREATE_STUDENT, name, rollNo, deptt, contact, address);
                    }
                    //If edit student option is selected from the dialog box, update student entry in the database
                    else if (extras != null && extras.get(BUTTON_CLICKED).equals(EDIT)) {
                        new AsyncProcess().execute(UPDATE_STUDENT, name, rollNo, deptt, contact, address);
                    }
                }
                break;
            case R.id.cancel_button:
                finish();
                break;
        }
    }

    //This class performs database operations asynchronously
    public class AsyncProcess extends AsyncTask<String, Integer, Object> {

        String operation;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //Show progress bar in a dialog
            showDialog(PROGRESS_BAR);
        }

        @Override
        protected Object doInBackground(String... params) {
            operation = params[0];

            //Puts the current thread to sleep
            for (int i = 0; i < 100; i++) {
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                //Publishes the progress inside a Progress Bar
                publishProgress(i);
            }
//When View Student or Edit Student option is selected, It first gets the student from the database
            if (operation.equals(GET_STUDENT) || operation.equals(EDIT_STUDENT)) {
                Student student = null;
                try {
                    DbController check = new DbController(AddStudent.this);
                    check.open();
//Receives a Student type object in the variable 'student' with the same roll number as provided in params[1]
                    student = check.getStudent(params[1]);
                    check.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return student;
            }
            //Creates a new student entry in the database
            else if (operation.equals(CREATE_STUDENT)) {
                Student student = new Student(params[1], params[2], params[3], params[4], params[5]);
                try {
                    DbController rowEntry = new DbController(AddStudent.this);
                    rowEntry.open();
                    //Creates a new student with the entered details
                    long result = rowEntry.createEntry(student);
                    rowEntry.close();
                    //If student is added successfully, return SUCCESSFUL
                    if (result > 0) {
                        return SUCCESSFUL;
                    }
                    //else return FAILED
                    else {
                        return FAILED;
                    }
                } catch (Exception e) {
                    //Do Nothing
                }
            }
            //Updates a student entry in the database
            else if (operation.equals(UPDATE_STUDENT)) {
                try {
                    DbController updateRow = new DbController(AddStudent.this);
                    updateRow.open();
                    //Updates the student with the corresponding details
                    long result = updateRow.updateEntry(params[1], params[2], params[3],
                            params[4], params[5]);
                    updateRow.close();
                    //If student entry is updated successfully, return SUCCESSFUL
                    if (result > 0) {
                        return SUCCESSFUL;
                    }
                    //else return FAILED
                    else {
                        return FAILED;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return FAILED;
        }

        @Override
        protected void onPostExecute(Object o) {
            dismissDialog(PROGRESS_BAR);
//After getting the student from the database, it displays the details for the user to view or edit
            if (operation.equals(GET_STUDENT) || operation.equals(EDIT_STUDENT)) {
                Student student = (Student) o;
                userName.setText(student.getName());
                userRollNo.setText(student.getRollNo());
                userDeptt.setText(student.getDeptt());
                userContact.setText(student.getContact());
                userAddress.setText(student.getAddress());
            }
//After successfully creating/updating student, it sends the name and roll number to the Main Activity for generating List view
            else if (operation.equals(CREATE_STUDENT) || operation.equals(UPDATE_STUDENT)) {
                String result = (String) o;
                if (result.equals(SUCCESSFUL)) {
                    Intent intent = new Intent();
                    intent.putExtra(NAME, name);
                    intent.putExtra(ROLL_NUMBER, rollNo);
                    setResult(RESULT_OK, intent);
                    finish();
                }
//While adding new student, if the roll number entered by the user already exists, notify the user of the same
                else {
                    Toast.makeText(AddStudent.this,
                            STUDENT_ALREADY_EXISTS,
                            Toast.LENGTH_LONG).show();
                }
            }
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            pDialog.setProgress(values[0]);
        }
    }
}
