package com.example.harshit.StudentDatabase.utils;

/**
 * Created by Harshit on 1/26/2015.
 */
public interface AppConstants {
    //Constants used in Main Activity
    int ADD_REQUEST_CODE = 2;
    int EDIT_REQUEST_CODE = 200;
    int POSITION_0 = 0;
    int POSITION_1 = 1;
    int POSITION_2 = 2;
    int PROGRESS_BAR = 0;
    String VIEW_ALL = "View";
    String ADAPTER = "adapter";
    String DELETE_STUDENT = "Delete";
    String VIEW_STUDENT = "view";
    String FETCHING_DATA = "Fetching data. Please wait...";
    String CHOOSE_OPTION  = "Choose an option...";
    String SELECT_OPTION = "Select an option";
    String SORT_BY_NAME = "Sort by Name";
    String SORT_BY_ROLL_NO = "Sort by Roll Number";
    String SUCCESS = "Operation Successful";
    int NUM_PAGES = 2;
    String LIST_VIEW = "List View";
    String GRID_VIEW = "Grid View";


    //Constants used in AddStudent activity
    String GET_STUDENT = "getStudent";
    String ADD_STUDENT = "addButton";
    String EDIT = "edit";
    String VIEW = "view";
    String BUTTON_CLICKED = "Button";
    String EDIT_STUDENT = "edit";
    String CREATE_STUDENT = "createEntry";
    String UPDATE_STUDENT = "updateStudent";
    String SUCCESSFUL = "success";
    String FAILED = "fail";
    String BACK = "BACK";
    String UPDATE = "UPDATE";
    String ROLL_NO = "RollNo";
    String FILL_EACH_FIELD = "Each field needs to be filled!";
    String ROLL_NO_CONDITION = "Roll Number cannot be Zero!";
    String AVOID_LEADING_SPACES = "Please Avoid any Leading Spaces!";
    String NAME = "name";
    String ROLL_NUMBER = "rollNo";
    String STUDENT_ALREADY_EXISTS = "A student with that roll number already exists!";
    String PLEASE_WAIT = "Please wait...";
    String EMPTY = "";
    String SPACE = " ";
    String HEADING = "Student Details";
    String HEADER = "Edit Details";

    //Constants used in Login Activity
    String CREDENTIALS = "Student Database Credentials";
    int PRIVATE_MODE = 0;
    String USERNAME = "userName";
    String PASSWORD = "password";
    String LOGIN_FAILED = "Login Failed...";
    String ENTER_CREDENTIALS = "Please enter username and Password";
    String CREDENTIALS_INCORRECT = "UserName/Password is incorrect";
    String OK = "OK";

    //Constants used in SpinnerFragment
    String SELECTED_SPINNER_ITEM = "Spinner item selected";

}
