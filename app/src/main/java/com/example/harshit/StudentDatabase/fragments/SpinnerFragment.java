package com.example.harshit.StudentDatabase.fragments;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.harshit.StudentDatabase.R;
import com.example.harshit.StudentDatabase.utils.AppConstants;

import java.util.ArrayList;
import java.util.List;

public class SpinnerFragment extends Fragment implements AppConstants {
    private Spinner spinner;
    private Bundle extras = new Bundle();
    OnDataPass dataPasser;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_spinner, container, false);
        addItemsOnSpinner(view);
        addListenerOnSpinnerItemSelection(view);
        return view;
    }

    public interface OnDataPass {
        public void onDataPass(Bundle extras);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            dataPasser = (OnDataPass) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnDataPass");
        }
    }

    //This function performs the necessary action once an item from the spinner is selected
    private void addListenerOnSpinnerItemSelection(View view) {
        spinner = (Spinner) view.findViewById(R.id.spinner);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == POSITION_1) {
                    extras.putInt(SELECTED_SPINNER_ITEM, POSITION_1);
                    dataPasser.onDataPass(extras);
                } else if (position == POSITION_2) {
                    extras.putInt(SELECTED_SPINNER_ITEM, POSITION_2);
                    dataPasser.onDataPass(extras);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });
    }

    //This function adds the items on the spinner
    private void addItemsOnSpinner(View view) {
        spinner = (Spinner) view.findViewById(R.id.spinner);
        List<String> spinnerItems = new ArrayList<>();
        spinnerItems.add(SELECT_OPTION); //Initial dummy entry
        spinnerItems.add(SORT_BY_NAME);
        spinnerItems.add(SORT_BY_ROLL_NO);
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, spinnerItems) {
            @Override
            //This is meant to hide the first item on the spinner
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = null;
                // If this is the initial dummy entry, make it hidden
                if (position == POSITION_0) {
                    TextView selectOption = new TextView(getContext());
                    selectOption.setHeight(0);
                    selectOption.setVisibility(View.GONE);
                    v = selectOption;
                } else {
                    // Pass convertView as null to prevent reuse of special case views
                    v = super.getDropDownView(position, null, parent);
                }
      // Hide scroll bar because it appears sometimes unnecessarily, this does not prevent scrolling
                parent.setVerticalScrollBarEnabled(false);
                return v;
            }
        };
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
    }


}

