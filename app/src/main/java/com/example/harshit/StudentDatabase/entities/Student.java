package com.example.harshit.StudentDatabase.entities;

import java.io.Serializable;

public class Student implements Serializable {
    private String name;
    String rollNo;
    private String deptt;
    private String contact;
    private String address;

    public Student(String name, String rollNo){
        this.setName(name);
        this.setRollNo(rollNo);
    }

    public Student(String name, String rollNo, String deptt,String contact, String address) {
        this.setName(name);
        this.setRollNo(rollNo);
        this.setDeptt(deptt);
        this.setContact(contact);
        this.setAddress(address);
    }

    public String getDeptt() {
        return deptt;
    }

    public void setDeptt(String deptt) {
        this.deptt = deptt;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getRollNo() {
        return rollNo;
    }

    public void setRollNo(String rollNo) {
        this.rollNo = rollNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
