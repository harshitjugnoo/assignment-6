package com.example.harshit.StudentDatabase.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.harshit.StudentDatabase.R;
import com.example.harshit.StudentDatabase.utils.AppConstants;

public class LoginActivity extends Activity implements View.OnClickListener, AppConstants {

    Button btnLogin;
    String userNamePref;
    String passwordPrefs;
    SharedPreferences credentials;
    SharedPreferences.Editor editor;
    EditText txtUserName, txtPassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        btnLogin = (Button) findViewById(R.id.btn_Login);
        txtUserName = (EditText) findViewById(R.id.txt_Username);
        txtPassword = (EditText) findViewById(R.id.txt_Password);
        credentials = getSharedPreferences(CREDENTIALS, PRIVATE_MODE);
        btnLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        AlertDialogManager alert = new AlertDialogManager();

        String userName = txtUserName.getText().toString();
        String passWord = txtPassword.getText().toString();


        editor = credentials.edit();
        userNamePref = credentials.getString(USERNAME, null);
        passwordPrefs = credentials.getString(PASSWORD, null);

        if (userName.trim().length() > 0 && passWord.trim().length() > 0) {

            //Shared Preferences' file is checked for any saved credentials
            if (credentials.contains(USERNAME)) {
                //If Shared preferences file contains some saved data then
                //The username and password entered are checked with those kept in shared preferences
                if (userName.equals(userNamePref) && passWord.equals(passwordPrefs)) {
                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(i);
                    finish();
                } else {
                    //An alert dialog is shown if the username/password entered are incorrect
                    alert.showAlertDialog(LoginActivity.this, LOGIN_FAILED, CREDENTIALS_INCORRECT, false);
                }
            }
            //This saves the credentials if the user signs up for the first time
            else {
                editor.putString(USERNAME, userName);
                editor.putString(PASSWORD, passWord);
                editor.commit();
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(i);
                finish();
            }
        } else {
            //This is the case when the username and password fields are still empty and user presses the login button
            alert.showAlertDialog(LoginActivity.this, LOGIN_FAILED, ENTER_CREDENTIALS, false);
        }


    }

    //The AlertDialogManager class contains a method that displays a dialog when it is required
    private class AlertDialogManager {
        public void showAlertDialog(Context context, String title, String message, Boolean status) {
            AlertDialog alertDialog = new AlertDialog.Builder(context).create();

            alertDialog.setTitle(title);
            alertDialog.setMessage(message);
            if (status != null) {
                alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);

            }
            alertDialog.setButton(OK, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            alertDialog.show();
        }
    }
}
