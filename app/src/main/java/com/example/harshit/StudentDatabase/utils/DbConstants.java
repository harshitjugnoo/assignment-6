package com.example.harshit.StudentDatabase.utils;

/**
 * Created by Harshit on 2/4/2015.
 */
public interface DbConstants {
    String DATABASE_NAME = "StudentDatabase";
    String DATABASE_TABLE = "StudentTable";
    int DATABASE_VERSION = 1;
    String KEY_NAME = "students_name";
    String KEY_ROLLNO = "students_rollNo";
    String KEY_DEPTT = "students_deptt";
    String KEY_ADDRESS = "students_address";
    String KEY_CONTACT = "students_contact";
}
