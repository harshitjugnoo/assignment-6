package com.example.harshit.StudentDatabase.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;

import com.example.harshit.StudentDatabase.R;
import com.example.harshit.StudentDatabase.activities.AddStudent;
import com.example.harshit.StudentDatabase.activities.MainActivity;
import com.example.harshit.StudentDatabase.adapters.MyCustomAdapter;
import com.example.harshit.StudentDatabase.utils.AppConstants;

import static com.example.harshit.StudentDatabase.activities.MainActivity.studentList;

/**
 * Created by Harshit on 2/23/2015.
 */
public class GridViewFragment extends Fragment implements AppConstants {

    private GridView gridView;
    private Button viewOption, editOption, deleteOption;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_grid_view, container, false);
        gridView = (GridView) view.findViewById(R.id.grid_view);

        if (getActivity() instanceof MainActivity) {
            ((MainActivity) getActivity()).new AsyncGetView().execute(VIEW_ALL);
        }
        if(getArguments()!=null){
            gridView.setAdapter((MyCustomAdapter) getArguments().getSerializable(ADAPTER));
        }

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                itemClick(parent, view, position, id);//function to display the dialog box
            }
        });
        return view;
    }

    private void itemClick(AdapterView<?> parent, View view, final int position, long id) {
        final Dialog dialog = new Dialog(getActivity()); //create the Dialog class object
        dialog.setContentView(R.layout.custom_dialog);
        dialog.setTitle(CHOOSE_OPTION); //Sets the title of the dialog
        viewOption = (Button) dialog.findViewById(R.id.viewOption);
        editOption = (Button) dialog.findViewById(R.id.editOption);
        deleteOption = (Button) dialog.findViewById(R.id.deleteOption);

        //for viewing details of a student
        viewOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent view = new Intent(getActivity(), AddStudent.class);
                String rollNo = studentList.get(position).getRollNo(); //The roll number is used to get data from database
                view.putExtra(ROLL_NO, rollNo);// So it is sent to the AddStudent activity
                view.putExtra(BUTTON_CLICKED, VIEW_STUDENT); //Bundle up the button which is clicked
                startActivity(view);
                dialog.dismiss();
            }
        });

        //for editing the details of the student
        editOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent edit = new Intent(getActivity(), AddStudent.class);
                String rollNo = studentList.get(position).getRollNo();//The roll number is used to get data from database
                edit.putExtra(ROLL_NO, rollNo);
                edit.putExtra(BUTTON_CLICKED, EDIT_STUDENT);//Bundle up the button which is clicked
                startActivityForResult(edit, EDIT_REQUEST_CODE);
                dialog.dismiss();
            }
        });

        //for deleting the student
        deleteOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String rollNo = studentList.get(position).getRollNo();//The roll number is used to delete student from database
                ((MainActivity) getActivity()).new AsyncGetView().execute(DELETE_STUDENT,
                        rollNo, Integer.toString(position));
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}
