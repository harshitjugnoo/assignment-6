package com.example.harshit.StudentDatabase.adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.harshit.StudentDatabase.fragments.GridViewFragment;
import com.example.harshit.StudentDatabase.fragments.ListViewFragment;
import com.example.harshit.StudentDatabase.utils.AppConstants;

/**
 * Created by Harshit on 2/23/2015.
 */
public class TabsPagerAdapter extends FragmentPagerAdapter implements AppConstants{
    Bundle bundle;

    public TabsPagerAdapter(FragmentManager fm, Bundle bundle) {
        super(fm);
        this.bundle = bundle;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                ListViewFragment listViewFragment = new ListViewFragment();
                listViewFragment.setArguments(bundle);
                return listViewFragment;
            case 1:
                GridViewFragment gridViewFragment = new GridViewFragment();
                gridViewFragment.setArguments(bundle);
                return gridViewFragment;
        }
        return null;
    }

    @Override
    public int getCount() {
        return NUM_PAGES;
    }
}
