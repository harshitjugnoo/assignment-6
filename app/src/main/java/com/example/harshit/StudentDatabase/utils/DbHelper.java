package com.example.harshit.StudentDatabase.utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Harshit on 2/4/2015.
 */
public class DbHelper extends SQLiteOpenHelper implements AppConstants, DbConstants {

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + DATABASE_TABLE + " (" +
                        KEY_NAME + " TEXT NOT NULL COLLATE NOCASE, " +
                        KEY_ROLLNO + " INTEGER PRIMARY KEY NOT NULL, " +
                        KEY_DEPTT + " TEXT NOT NULL, " +
                        KEY_ADDRESS + " TEXT NOT NULL, " +
                        KEY_CONTACT + " TEXT NOT NULL);"
        );
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE);
        onCreate(db);
    }
}
