package com.example.harshit.StudentDatabase.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.harshit.StudentDatabase.entities.Student;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by Harshit on 2/4/2015.
 */
public class DbController implements DbConstants {
    private DbHelper ourHelper;
    private Context ourContext;
    private SQLiteDatabase ourDatabase;

    public DbController(Context context) {
        ourContext = context;
    }

    public DbController open() throws SQLException {
        ourHelper = new DbHelper(ourContext);
        ourDatabase = ourHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        ourHelper.close();
    }

    public long createEntry(Student student) {

            ContentValues fields = new ContentValues();
            fields.put(KEY_NAME, student.getName());
            fields.put(KEY_ROLLNO, student.getRollNo());
            fields.put(KEY_DEPTT, student.getDeptt());
            fields.put(KEY_CONTACT, student.getContact());
            fields.put(KEY_ADDRESS, student.getAddress());
        try {
            return ourDatabase.insert(DATABASE_TABLE, null, fields);
        } catch (Exception e) {
            return -1;
        }
    }

    public ArrayList getData() {
        ArrayList<Student> studentList = new ArrayList<>();
        String[] columns = new String[]{KEY_NAME, KEY_ROLLNO, KEY_DEPTT, KEY_CONTACT, KEY_ADDRESS};
        Cursor cursor = ourDatabase.query(DATABASE_TABLE, columns, null, null, null, null, null);

        int iName = cursor.getColumnIndex(KEY_NAME);
        int iRollNo = cursor.getColumnIndex(KEY_ROLLNO);
        int iDeptt = cursor.getColumnIndex(KEY_DEPTT);
        int iContact = cursor.getColumnIndex(KEY_CONTACT);
        int iAddress = cursor.getColumnIndex(KEY_ADDRESS);

        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            studentList.add(new Student(cursor.getString(iName), cursor.getString(iRollNo), cursor.getString(iDeptt),
                    cursor.getString(iContact), cursor.getString(iAddress)));
        }
        return studentList;
    }

    public Student getStudent(String rollNo) {
        String[] columns = new String[]{KEY_NAME, KEY_ROLLNO, KEY_DEPTT, KEY_CONTACT, KEY_ADDRESS};
        Cursor cursor = ourDatabase.query(DATABASE_TABLE, columns, KEY_ROLLNO + "=" + rollNo, null, null, null, null);
        Student student;
        int iName = cursor.getColumnIndex(KEY_NAME);
        int iRollNo = cursor.getColumnIndex(KEY_ROLLNO);
        int iDeptt = cursor.getColumnIndex(KEY_DEPTT);
        int iContact = cursor.getColumnIndex(KEY_CONTACT);
        int iAddress = cursor.getColumnIndex(KEY_ADDRESS);
        if (cursor != null) {
            cursor.moveToFirst();
            student = new Student(cursor.getString(iName), cursor.getString(iRollNo), cursor.getString(iDeptt),
                    cursor.getString(iContact), cursor.getString(iAddress));
            return student;
        }
        return null;
    }
    public ArrayList<Student> getStudentOnSearch(String name){
        ArrayList<Student> studentsMatched = new ArrayList<>();
        String[] columns = new String[]{KEY_NAME, KEY_ROLLNO};
        /*String searchQuery = "SELECT * FROM " + DATABASE_TABLE + " WHERE " + KEY_NAME + "=" + name;
        Cursor cursor = ourDatabase.rawQuery(searchQuery, null);*/
        Cursor cursor = ourDatabase.rawQuery("SELECT * FROM " + DATABASE_TABLE +
                " WHERE "+KEY_NAME+" LIKE '%"+name+"%'",null);
        int iName = cursor.getColumnIndex(KEY_NAME);
        int iRollNo = cursor.getColumnIndex(KEY_ROLLNO);
        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()){
            studentsMatched.add(new Student(cursor.getString(iName), cursor.getString(iRollNo)));
        }
        return studentsMatched;
    }

    public long updateEntry(String name, String rollNo, String deptt, String contact, String address) {
        ContentValues updateValues = new ContentValues();
        updateValues.put(KEY_NAME, name);
        updateValues.put(KEY_DEPTT, deptt);
        updateValues.put(KEY_CONTACT, contact);
        updateValues.put(KEY_ADDRESS, address);
        return ourDatabase.update(DATABASE_TABLE, updateValues, KEY_ROLLNO + "=" + rollNo, null);
    }

    public void deleteStudent(String rollNo) {
        ourDatabase.delete(DATABASE_TABLE, KEY_ROLLNO + "=" + rollNo, null);
    }
}
