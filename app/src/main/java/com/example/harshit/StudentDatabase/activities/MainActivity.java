package com.example.harshit.StudentDatabase.activities;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.example.harshit.StudentDatabase.R;
import com.example.harshit.StudentDatabase.adapters.MyCustomAdapter;
import com.example.harshit.StudentDatabase.adapters.TabsPagerAdapter;
import com.example.harshit.StudentDatabase.entities.Student;
import com.example.harshit.StudentDatabase.fragments.GridViewFragment;
import com.example.harshit.StudentDatabase.fragments.ListViewFragment;
import com.example.harshit.StudentDatabase.fragments.NavigationDrawerFragment;
import com.example.harshit.StudentDatabase.fragments.SpinnerFragment;
import com.example.harshit.StudentDatabase.utils.AppConstants;
import com.example.harshit.StudentDatabase.utils.DbController;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class MainActivity extends ActionBarActivity implements Serializable, AppConstants,
        SpinnerFragment.OnDataPass, android.support.v7.app.ActionBar.TabListener,
        NavigationDrawerFragment.NavigationDrawerCallbacks, ListViewFragment.OnListPass {
    private MyCustomAdapter adaptor;
    transient private ProgressDialog pDialog;
    transient private ViewPager viewPager;
    transient private TabsPagerAdapter tabsPagerAdapter;
    //List for maintaining the Student class objects
    public static ArrayList<Student> studentList = new ArrayList<>();
    transient android.support.v7.app.ActionBar.Tab listViewTab;
    transient android.support.v7.app.ActionBar.Tab gridViewTab;
    transient Bundle bundle = new Bundle();
    transient Fragment listFragment = new ListViewFragment();
    transient Fragment gridFragment = new GridViewFragment();
    transient android.support.v7.app.ActionBar actionBar;
    transient private NavigationDrawerFragment mNavigationDrawerFragment;
    transient private CharSequence mTitle;
    transient private ArrayList<Student> studentSearch = new ArrayList<>();
    transient ListView filterListView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeComponents();
        addListenerOnViewPager();
    }

    private void initializeComponents() {
        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
        //Initialize the adapter, put it in a bundle and send it to List View and Grid View Fragments
        adaptor = new MyCustomAdapter(MainActivity.this, new ArrayList<Student>());
        bundle.putSerializable(ADAPTER, adaptor);
        listFragment.setArguments(bundle);
        gridFragment.setArguments(bundle);


        viewPager = (ViewPager) findViewById(R.id.view_pager);
        tabsPagerAdapter = new TabsPagerAdapter(getSupportFragmentManager(), bundle);

        viewPager.setAdapter(tabsPagerAdapter);
        actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        listViewTab = actionBar.newTab().setText(LIST_VIEW).setTabListener(this);
        gridViewTab = actionBar.newTab().setText(GRID_VIEW).setTabListener(this);
        actionBar.addTab(listViewTab);
        actionBar.addTab(gridViewTab);
    }

    private void addListenerOnViewPager() {
        viewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                actionBar.setSelectedNavigationItem(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }


    // This is meant to get data from SpinnerFragment and
    // sort the students on the basis of key returned
    @Override
    public void onDataPass(Bundle extras) {
        if (extras.getInt(SELECTED_SPINNER_ITEM) == (POSITION_1)) {
            Collections.sort(studentList, new StudentNameComp());
            adaptor.notifyDataSetChanged();
        } else if (extras.getInt(SELECTED_SPINNER_ITEM) == (POSITION_2)) {
            Collections.sort(studentList, new StudentRollNoComp());
            adaptor.notifyDataSetChanged();
        }
        Log.d("LOG", "Spinner Item Selected " + extras.getInt(SELECTED_SPINNER_ITEM));
    }


    @Override
    public void onTabSelected(ActionBar.Tab tab, android.support.v4.app.FragmentTransaction fragmentTransaction) {
        int position = tab.getPosition();
        viewPager.setCurrentItem(position);
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, android.support.v4.app.FragmentTransaction fragmentTransaction) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, android.support.v4.app.FragmentTransaction fragmentTransaction) {

    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, PlaceholderFragment.newInstance(position + 1))
                .commit();
    }

    @Override
    public void onListPass(ListView listView1) {
        filterListView = listView1;
    }

    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }


        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            ((MainActivity) activity).onSectionAttached(
                    getArguments().getInt(ARG_SECTION_NUMBER));
        }
    }

    // Restoring Action Bar
    public void restoreActionBar() {
        actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_action_view, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchableInfo info = searchManager.getSearchableInfo(getComponentName());
        searchView.setSearchableInfo(info);
        searchView.setQueryHint(getString(R.string.search_hint));



        MenuItemCompat.setOnActionExpandListener(searchItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                adaptor.list = studentList;
                adaptor.notifyDataSetChanged();
                return true;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
//                Toast.makeText(MainActivity.this, s, Toast.LENGTH_SHORT).show();
                DbController getData = new DbController(MainActivity.this);
                try {
                    getData.open();
                    studentSearch = getData.getStudentOnSearch(s);
                    getData.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                adaptor.list = studentSearch;
                adaptor.notifyDataSetChanged();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                loadHistory(s);
                return true;
            }
        });
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return true;
    }

    private void loadHistory(String query) {


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mTitle = getString(R.string.home);
                homeView();
                break;
            case 2:
                mTitle = getString(R.string.add_student);
                openAddStudent();
                break;
            case 3:
                mTitle = getString(R.string.logout);
                logout();
                break;
            default:
                break;
        }
    }

    private void homeView() {
        adaptor.list = studentList;
        adaptor.notifyDataSetChanged();
    }

    void openAddStudent() {
        Intent intent = new Intent(MainActivity.this, AddStudent.class);
        intent.putExtra(BUTTON_CLICKED, ADD_STUDENT);
        startActivityForResult(intent, ADD_REQUEST_CODE);// Activity is started with requestCode = ADD_REQUEST_CODE

    }

    private void logout() {
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    //Custom comparator for sorting students on the basis of names
    class StudentNameComp implements Comparator<Student> {

        @Override
        public int compare(Student lhs, Student rhs) {
            return lhs.getName().compareToIgnoreCase(rhs.getName());
        }
    }

    //Custom comparator for sorting students on the basis of roll numbers
    class StudentRollNoComp implements Comparator<Student> {

        @Override
        public int compare(Student lhs, Student rhs) {
            if (Integer.parseInt(lhs.getRollNo()) < Integer.parseInt(rhs.getRollNo())) {
                return -1;
            } else {
                return 1;
            }
        }
    }

    //This function is used to receive a result from an activity
    //In this case, it receives name and roll number of the corresponding student
    //It then adds it to the adapter for generating list view
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //If the request code is ADD_REQUEST_CODE, add the new student to the adapter's list
        if (requestCode == ADD_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                String name = data.getStringExtra(NAME);
                String rollNumber = data.getStringExtra(ROLL_NUMBER);
                adaptor.list.add(new Student(name, rollNumber)); //Adds the student to the studentList and the adapter
                adaptor.notifyDataSetChanged();
                Toast.makeText(MainActivity.this, SUCCESS, Toast.LENGTH_SHORT).show();
            }
        }
        //If the request code is EDIT_REQUEST_CODE, add the updated student to the adapter's list
        else if (requestCode == EDIT_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                String name = data.getStringExtra(NAME); //Fetches the edited data
                String rollNumber = data.getStringExtra(ROLL_NUMBER);
                int i;
                //It first finds the position of the student with the help of roll number
                for (i = 0; i < studentList.size(); i++) {
                    if (studentList.get(i).getRollNo().equals(rollNumber))
                        break;
                }
                adaptor.list.set(i, new Student(name, rollNumber)); ////Sets the student to the list of the adapter
                adaptor.notifyDataSetChanged();
                Toast.makeText(MainActivity.this, SUCCESS, Toast.LENGTH_SHORT).show();
            }
        }
    }

    //Shows the Progress Bar Dialog
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case PROGRESS_BAR:
                pDialog = new ProgressDialog(this);
                pDialog.setMessage(FETCHING_DATA);
                pDialog.setIndeterminate(false);
                pDialog.setMax(100);
                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pDialog.setCancelable(false);
                pDialog.show();
                return pDialog;
            default:
                return null;
        }
    }

    //This is a subclass of AsyncTask. This class does the database operations Asynchronously.
    public class AsyncGetView extends AsyncTask<String, Integer, ArrayList> {
        String selectedOption;
        int position;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(PROGRESS_BAR);
        }

        @Override
        protected ArrayList doInBackground(String... params) {
            selectedOption = params[0];


            //Puts the current thread to sleep
            for (int i = 0; i < 100; i++) {
                try {
                    Thread.sleep(8);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                //Publishes the progress inside a Progress Bar
                publishProgress(i);
            }
            //When view all is selected, it gets all the students from the database in the form of a list
            if (selectedOption.equals(VIEW_ALL)) {
                DbController getDatabase = new DbController(MainActivity.this);
                try {
                    getDatabase.open();
                    //Receives all the students in an ArrayList
                    studentList = getDatabase.getData();
                    getDatabase.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                return studentList;
            }
            //If delete option is selected, the student with the corresponding roll number is deleted
            else if (selectedOption.equals(DELETE_STUDENT)) {
                position = Integer.parseInt(params[2]);
                DbController deleteEntry = new DbController(MainActivity.this);
                try {
                    deleteEntry.open();
                    //params[1] contains Roll number of the student which is to be deleted
                    deleteEntry.deleteStudent(params[1]);
                    deleteEntry.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(ArrayList arrayList) {
            dismissDialog(PROGRESS_BAR);
            adaptor.list = studentList;
            if (selectedOption.equals(DELETE_STUDENT)) {
                adaptor.list.remove(position);
            }
            adaptor.notifyDataSetChanged();
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            pDialog.setProgress(values[0]);
        }
    }
}


